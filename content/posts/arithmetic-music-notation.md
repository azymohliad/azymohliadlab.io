---
title: "[Draft] Exploring Music Theory through the Alternative Notation"
date: 2020-06-09T20:33:06+03:00
draft: true
math:
    enable: true
---

This article is written by a curious beginner in music for people alike. It aims to give another opinionated but hopefully interesting perspective into some concepts of music theory. I wrote the initial version in one night after a spark of inspiration, but I likely won't consider it ready for months, I still need to think many aspects of it through. So I'll leave this draft publicly available here, it won't let me just forget about it. But I won't actively share it either for now.


## Intro

Music involves a lot of arithmetic: counting which notes belong to a particular chord, finding notes and intervals on an instrument, transposing a song to another key, building scales and chord progressions in a particular key, etc.

However, the standard music notation (particularly, the naming of notes, intervals and chords) isn’t really optimized for arithmetic. I guess it feels quite comfortable with piano, but I think it isn't very versatile.

Here I want to propose and explore the alternative, arithmetic notation. I believe that it is easier to use and it also helps to understand the internal structure of some music theory concepts better than the standard one.


## Crash course on equal temperament

Since the 18th century, the most common standard way to define notes and relations between their pitches is **12-tone [equal temperament](https://en.wikipedia.org/wiki/Equal_temperament) (12-TET)**. The minimum interval between two adjacent notes is called **semitone** or **half-step**, 12 such semitones form an **octave**.

Diving into tech details, any musical interval is a ratio between sound frequencies (not a difference). Octave is the most stable natural interval and its frequency ratio is 2, so shifting one octave up means doubling the frequency. Nerding on it deeper, since there are 12 semitones in an octave, multiplying by semitone’s ratio 12 times should give an octave ratio, hence semitone’s ratio is $2^{1/12}$.

So, mathematically the whole range of musical notes' frequencies is just a geometric progression with a ratio of $2^{1/12}$, and if you define one base frequency (the standard is A4=440Hz), you can calculate the frequency of the note $N$ semitones up by multiplying it by $2^{N/12}$. Beautiful!

Equal temperament is very practical in music, it allows transposing the key (shifting all pitches in the composition) by any arbitrary number of semitones. And it generally allows us to easily write multi-octave music. Pre-12-TET tuning systems produced so-called [“wolf intervals”](https://en.wikipedia.org/wiki/Wolf_interval) - dissonant intervals, caused by pitch mismatch by getting to the same note with multiple various intervals (e.g. 12 fifths vs 7 octaves).

## Notes

The scale (set of notes ordered by pitch) that contains all 12 notes per octave is called the **chromatic** scale. There are also many different kinds of scales that contain only a subset of these notes. Very popular in western music are **diatonic** scales: scales that contain 7 notes per octave with 5 whole tones and 2 semitones between them.

And this is where the standard notation is starting to become weird.

These 7 notes of the particular diatonic scale “C major” got proper names: **C (do), D (re), E (mi), F (fa), G (sol), A (la), B (ti)**. And the remaining 5 notes in the chromatic scale are marked with their neighbor’s name and shifted a semitone up (sharp, ♯) or down (flat, ♭). All together: **C, C#, D, D#, E, F, F#, G, G#, A, A#, B**.

![Btw, the ratio between grayscale tones of 2 adjacent notes here is 2^1/12 too :)](/images/notes_standard.svg "Circular visualization of notes in standard notation")

The reason for this, as I understand, is that diatonic scales are much older than chromatic, they were around long before we had 12-TET (built by chaining 6 perfect fifths, interval with natural ratio 3/2). There were indeed only 7 notes when they got their names, so historically it totally makes sense, but now, in a chromatic world it feels inconsistent at least.

The bigger problem with this notation is that it’s inconvenient to do arithmetic on. Suppose you want to transpose the key of the song by one semitone up: C becomes C#, C# becomes D, D becomes D#, etc, but E becomes F, and B becomes C. You need to remember that E from F and B from C are one semitone apart, but other adjacent notes of C major scale are the whole tone apart from each other. It becomes even more unwieldy if you need to shift by more, like 2-3 whole tones.

The obvious solution: let’s just name notes starting from C by numbers from $0$ to $11$ (starting from 0 because it’s friendly with modular arithmetic):

| C  | C# | D  | D# | E  | F  | F# | G  | G# | A  | A# | B  |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| 0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 |

So our circular visualization of notes turns almost into a wall clock: 

![Btw, the ratio between grayscale tones of 2 adjacent notes here is 2^1/12 too :)](/images/notes_arithmetic.svg "Circular visualization of notes in arithmetic notation")

Diatonic scales would be defined as some 7 numbers, for example, C major is $0,2,4,5,7,9,11$. And whenever we need to transpose the note, we just add the number of semitones to it, and every time it reaches 12 it wraps around to 0. Mathematically speaking we are performing modular sum in modulo 12. For example:


$$
0 + 2 = 2 \pmod {12} \newline
11 + 2 = 13 = 1 \pmod {12}
$$

Since notes are repeated periodically every octave, modular arithmetic will be used a lot.

Another example, transposing C major by 5 semitones would be:

$$
0,2,4,5,7,9,11 + 5 = 5,7,9,10,12,14,16 = 5,7,9,10,0,2,4 \pmod {12}
$$

By standard notation, this is now F major scale.

Note, for the sake of simplicity I don’t use proper math notation here, we're applying the operator to each comma-separated number.

Whenever we need to define notes more rigorously, we can add the octave number as a subscript or in parentheses, e.g. $A4 = 9_4$. But the rest of this article describes use-cases where it isn't needed.

Another idea I had is to represent notes in [duodecimal system](https://en.wikipedia.org/wiki/Duodecimal) (numeral system with the base of 12). It would allow to encode octave and note as a single 2-digits number, where the 1st digit (the most significant one) would always represent the octave, and the 2nd digit would always be the note. And we wouldn't need modular arithmetic here. But for our brains, trained their whole life on a decimal system, that would probably be more difficult to read and interpret than just occasionally doing sum in modulo 12.

## Intervals

To me, intervals always seemed to have confusing names in terms of their magnitudes in semitones.

| Name             | Magnitude |
|:----------------:|:---------:|
| Perfect unison   |   0       |
| Minor second     |   1       |
| Major second     |   2       |
| Minor third      |   3       |
| Major third      |   4       |
| Perfect fourth   |   5       |
| Augmented fourth |   6       |
| Perfect fifth    |   7       |
| Minor sixth      |   8       |
| Major sixth      |   9       |
| Minor seventh    |   10      |
| Major seventh    |   11      |
| Perfect octave   |   12      |

Again, historically it makes sense because these names were derived from distances in the major diatonic scale. But for our arithmetic notation, let’s drop their names and use their magnitudes in semitones as identifiers. To distinguish intervals from notes, let’s always put intervals in square brackets, e.g. perfect fifth would be $[7]$.

## Chords

[Chords naming scheme](https://en.wikipedia.org/wiki/Chord_(music)#Notation_in_popular_music) is pretty complex, in short it has the following items:

- A root note name, e.g. `C`
- *(optional)* Symbol or abbreviation indicating the chord quality (minor, augmented, diminished, etc), e.g. `m` for minor
- *(optional)* Number(s) indicating stacked interval above the root note, e.g. `7` for minor seventh
- *(optional)* Additional musical symbols for special alterations, e.g. `sus2` or `add13`
- *(optional)* Added `/` followed by another note's name, indicating that it should be played instead of the root note.

For example: C, F#, Gm, or more complex: BMaj7, Asus4.

AFAIK, any legit combination of those optional markers always maps to a single list of intervals built from the root note (except added `/` which replaces the root note).

So in our arithmetic notation we could name chords with the following simple scheme: $X[i_1,i_2,...]$, where:

- $X$ is a root note
- $[i_1,i_2,...]$ is an explicit list of intervals (excluding root note unison itself $[0]$, there’s no point duplicating it here since it’s the part of the chord anyway)

For example, a simple major triad is built from the root note, $[4]$ (major third) and $[7]$ (perfect fifth). So the interval signature for any major triad would be $[4,7]$, coupled with the root note it would be like $0[4,7]$ (for C) or $6[4,7]$ (for F#). Minor triad is built from the root note, $[3]$ (minor third) and $[7]$, so its interval signature would be $[3,7]$. $sus4$ is $[5,7]$, $sus2$ is $[2,7]$, and so on.

Here are some more examples:


| Standard signature | Arithmetic signature | Example chords     | Same chords in standard notation |
|:------------------:|:--------------------:|:------------------:|:--------------------------------:|
| X                  | $X[4,7]$             | $0[4,7]$, $7[4,7]$ | C, G                             |
| Xm                 | $X[3,7]$             | $9[3,7]$, $2[3,7]$ | Am, Dm                           |
| X6                 | $X[4,7,9]$           | $5[4,7,9]$         | E#6                              |
| X7                 | $X[4,7,10]$          | $7[4,7,10]$        | G7                               |
| Xsus2              | $X[2,7]$             | $5[2,7]$           | Fsus2                            |
| Xsus4              | $X[5,7]$             | $6[5,7]$           | F#sus4                           |

Using this chord notation also forces memorizing the structure of the chords.

Since the root note is isolated from an interval signature, it is very easy to transpose the key in this notation. Just add the interval you’re transposing by to all the root notes in your chord progressions and you will get transposed chord progressions.

How to find the exact set of notes belonging to the chord? Take the root note, and for each interval in its signature add them together in modulo 12. Don’t forget to keep the root note too. For example:

$$
Notes(7[4,7,10]) = 7 + [0,4,7,10] = 7,11,14,17 = 7,11,2,5 \pmod {12}
$$

Finally, chords with added `/` are a little tricky to convert between notations (I blame inconsistency in the standard notation because the first letter in the chord name doesn’t indicate the root note anymore). Let's take D/E for example. D is $2[4,7]$, so the notes it consists of are $2,6,9$. D/E should have all the same notes, plus $4$ (E) as a new root note: $4,2,6,9$. Now we need to do the inverse transform from notes to interval signature, i.e. calculate the intervals from $4$ to $2,6,9$. For that we just subtract $4$ from other notes in modulo 12:

$$
[2,6,9] - 4 = [-2,2,5] = [10,2,5] \pmod {12}
$$

Since these aren’t root notes, we are free to reorder them, so we can write it as $4[2,5,10]$.

Of course, this chord notation wouldn’t be practical to use if you had to perform these conversions every time you need to play a chord. But the idea is that with time you develop muscle memory for each chord signature (same as you do with the standard notation). But whenever you don’t have a chord in your muscle memory, it’s pretty straightforward to construct it.

## A little practice: guitar fretboard

Instruments with the “chromatic UI” are where this notation shines the most. For example, for guitar we only need to remember the note of every open string:

| E (6th) | A (5th) | D (4th) | G (3rd) | B (2nd) | E (1st) |
|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
|  $4_2$  |  $9_2$  |  $2_3$  |  $7_3$  |  $11_3$ |  $4_4$  |

And then we can identify any note on the fretboard as the sum of an open string note ($X$) and the fret number ($N$) modulo 12:

$$
X + N \mod {12}
$$

For example, 4th string 8th fret is $2 + 8 = 10 \pmod {12}$ (A#).

![Fretboard](/images/fretboard.svg "Identifying notes on the fretboard")

Now, suppose you want to play the chord $4[4,7,9]$ (E6). You calculate its notes:

$$
4 + [0,4,7,9] = 4,8,11,2 \pmod {12}
$$

Then you can easily find these notes on a fretboard, they just need to be equal to the modular sum of an open string note and a fret number.

To not deal with modular arithmetic and calculate faster on the fly, the trick is: add 12 to the note you’re looking for, and then subtract the open string note from it - this is your fret number. Then look if you can move 12 frets up/down from it to find other occurrences of the same note on the same string.

For example, let’s find all notes $4$ (E) on the fretboard. Let’s add 12 to it first: $4+12=16$, then subtract open strings' notes from it:

$$
16 - \begin{vmatrix}
    4  \cr
    11 \cr
    7  \cr
    2  \cr
    9  \cr
    4
\end{vmatrix} = \begin{vmatrix}
    12 \cr
    5  \cr
    9  \cr
    14 \cr
    7  \cr
    12
\end{vmatrix} \rarr \begin{vmatrix}
    0 & 12 \cr
    7 & 19 \cr
    2 & 14 \cr
    9 & 21 \cr
    5 & 17 \cr
    0 & 12 \cr
\end{vmatrix}
$$

The resulting matrix lists all notes $4$ on a fretboard (read it like a tab).

Repeat it for every note in a chord ($4,8,11,2$) and find the combination that is comfortable for your fingers, just remember that the lowest pitch note should be the root one.

It may look like a lot of mental work, but it's very easy to get used to, and it's much easier than memorizing fretboard by the standard notes' names.

One more fun (but useless) thing you can do with a guitar is to convert tabs into notes by adding the vector of open strings' notes.


## Deeper into theory: diatonic modes

[Diatonic modes](https://en.wikipedia.org/wiki/Diatonic_scale#Modes) are a great example of human wisdom in music. They are a framework that lets you write chord progressions and arpeggios in a particular mood/flavor and always sound consonant.

All diatonic scales are formed by 5 whole tones (T) and 2 semitones (S) in a different order. The major scale is the following: `T-T-S-T-T-T-S`. This interval sequence is also called “**Ionian mode**”. If you start from the second note (rotate the sequence by one symbol), you’ll get “**Dorian mode**”: `T-S-T-T-T-S-T`. And so on. In total there are 7 modes you can build this way:

| Mode       | Scheme          | Comment          |
|:----------:|:---------------:|:----------------:|
| Ionian     | `T–T–S–T–T–T–S` | aka major scale  |
| Dorian     | `T–S–T–T–T–S–T` |                  |
| Phrygian   | `S–T–T–T–S–T–T` |                  |
| Lydian     | `T–T–T–S–T–T–S` |                  |
| Mixolydian | `T–T–S–T–T–S–T` |                  |
| Aeolian    | `T–S–T–T–S–T–T` | aka minor scale  |
| Locrian    | `S–T–T–S–T–T–T` |                  |

On piano you can play them all just by playing 7 adjacent white keys, shifting the starting key by one for each mode.

If you're new to modes and want to quickly see if they work, search for a backing track on youtube (or anywhere) in particular mode and key, derive the notes, and play along to the backing track improvising using only those notes.

For example, let’s say you want to play in a D Aeolian. Which notes are you "allowed" to play? If you count Aeolian intervals from D, you’ll get these: D, E, F, G, A, A#, C.

We can also build chords from those notes. Arithmetic notation can help here too to derive all "allowed" chords faster.

First, let's represent the modes as the list of intervals from the root note (and btw, thanks a lot for reading this far!):

| Mode      | Intervals          |
|:---------:|:------------------:|
|Ionian     | $[0,2,4,5,7,9,11]$ |
|Dorian     | $[0,2,3,5,7,9,10]$ |
|Phrygian   | $[0,1,3,5,7,8,10]$ |
|Lydian     | $[0,2,4,6,7,9,11]$ |
|Mixolydian | $[0,2,4,5,7,9,10]$ |
|Aeolian    | $[0,2,3,5,7,8,10]$ |
|Locrian    | $[0,1,3,5,6,8,10]$ |

D Aeolian is $[0,2,3,5,7,8,10]$ from the root note $2$. The notes we derived above can be calculated as usual:

$$
2 + [0,2,3,5,7,8,10] = 2,4,5,7,9,10,0 \pmod {12}
$$

Now to derive the chords in our arithmetic notation we need to know which intervals they contain. Let’s calculate intervals from each of these notes to the rest:

$$
\begin{vmatrix} 2 & 4 & 5 & 7 & 9 & 10 & 0 \end{vmatrix} -
\begin{vmatrix} 2 \cr 4 \cr 5 \cr 7 \cr 9 \cr 10 \cr 0 \end{vmatrix} =
\begin{bmatrix}
    0 & 2 & 3 & 5 & 7 & 8 & 10 \cr
    10 & 0 & 1 & 3 & 5 & 6 & 8 \cr
    9 & 11 & 0 & 2 & 4 & 5 & 7 \cr
    7 & 9 & 10 & 0 & 2 & 3 & 5 \cr
    5 & 7 & 8 & 10 & 0 & 1 & 3 \cr
    4 & 6 & 7 & 9 & 11 & 0 & 2 \cr
    2 & 4 & 5 & 7 & 9 & 10 & 0 \cr
\end{bmatrix} \pmod {12}
$$

We can use subsets of those intervals as chord signatures. For example, major chords ($[4,7]$) can only be built from $5$ (F), $10$ (A#) and $0$ (C) beause only these notes contain $[4]$ and $[7]$ in their lists of intervals. This way we can find many other chords that fits our mode and key:

| Note (standard) | Note (arithmetic) | Intervals          | Example chords (arithmetic)       | Same chords (standard) |
|:---------------:|:-----------------:|:------------------:|:---------------------------------:|:----------------------:|
| D               | $2$               | $[0,2,3,5,7,8,10]$ | $2[3,7]$, $2[2,7]$, $2[3,7,10]$   | Dm, Dsus2, Dm7         |
| E               | $4$               | $[10,0,1,3,5,6,8]$ | $2[3,5,10]$                       |                        |
| F               | $5$               | $[9,11,0,2,4,5,7]$ | $5[4,7]$, $5[5,7]$, $5[4,7,9]$    | F, Fsus4, F6           |
| G               | $7$               | $[7,9,10,0,2,3,5]$ | $7[3,7]$, $7[2,7]$, $7[5,7]$      | Gm, Gsus2, Gsus4       |
| A               | $9$               | $[5,7,8,10,0,1,3]$ | $9[3,7]$, $9[5,7]$, $9[3,7,10]$   | Am, Asus4, Am7         |
| A#              | $10$              | $[4,6,7,9,11,0,2]$ | $10[4,7]$, $10[2,7]$, $10[4,7,9]$ | A#, A#sus2, A#6        |
| C               | $0$               | $[2,4,5,7,9,10,0]$ | $0[4,7]$, $0[2,7], $0[5,7]$,      | C, Csus2, Csus4        |

From the table above you can also see that no simple chord can be built from the root $4$ (E) because there's no $[7]$ (perfect fifth) from it to any other note in the mode.



## Cirle of fifths

TODO

## Conclusion

So if we write:

- Notes as numbers from $0$ to $11$
- Intervals as their magnitude in semitones, e.g. $[7]$ (square brackets to distinguish from notes)
- Chords as a combination of root note and explicit intervals it consist of, e.g. $2[4,7]$

then we can perform transposition using modular arithmetic, easily find unfamiliar chords on the instruments, understand the internal structure of chords better, derive chords that work in particular mode and key easier... And hopefully this list is open.

To me, another great quality of this arithmetic notation is that to be useful it doesn’t need to be generally accepted. You may find it helpful even if you’re the only person knowing about it.

I think I will keep using and exploring it, and see if it can help to simplify any other aspects of music theory for me. And of course, you are welcome to do it too!